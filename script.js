
let num = 2;
let getCube = num ** 3;

console.log(`The cube of ${num} is ${getCube}`);

let address = ["258 Washington Ave NW", "California", "90011"];

let [street, city, zipCode] = address;

console.log(`I live at ${street}, ${city} ${zipCode}`);

let animal = {
	name: "Lolong",
	type: "salt water crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}

let {name, type, weight, measurement} = animal;

console.log(`${name} was a ${type}. He weighed ${weight} with a measurement of ${measurement}.`);

let numbers = [1, 2, 3, 4, 5];

numbers.forEach((number) => console.log(`${number}`));

let reducedArray = numbers.reduce((x, y) => x + y);
console.log(reducedArray);

class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

let myDog = new Dog("Frankie", 5, "Miniature Dachshund");

console.log(myDog);